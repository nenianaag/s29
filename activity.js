db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "88801234",
			email: "jane@mail.com"
	},
		courses: ["Python", "React", "PHP"],
		department: "HR"
	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "8880111",
			email: "stephen@mail.com"
	},
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "89805678",
			email: "neil@mail.com"
	},
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	},

])

// 2. Find users with letter s in their first name or d in their last name.

db.users.find({
	$or: [
		{ firstName: { $regex: "s", $options: "$i" } },
		{ lastName: { $regex: "d", $options: "$i" } },
	]
}, {firstName: 1, lastName: 1, _id: 0} )

// 3. Find users who are from the HR department and their age is greater than or equal to 70.

db.users.find({
	$and: [
		{
			department: "HR"	
		},
		{
			age: { $gte: 70 }
		}
	]
})

// 4. Find users with letter e in their first name and has an age of less than or equal to 30.

db.users.find({
	$and: [
		{
			firstName: { $regex: "e"}
		},
		{
			age: { $lte: 30 }
		}
	]
})
